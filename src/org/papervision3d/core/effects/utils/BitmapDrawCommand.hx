/**
* ...
* @author Default
* @version 0.1
*/

package org.papervision3d.core.effects.utils;

import openfl.display.BitmapData;
import openfl.display.BlendMode;
import openfl.display.DisplayObject;
import openfl.geom.ColorTransform;
import openfl.geom.Matrix;
import openfl.geom.Rectangle;

class BitmapDrawCommand {
	
	public var colorTransform:ColorTransform = null;
	public var transformMatrix:Matrix = null;
	public var blendMode:String = BlendMode.NORMAL;
	public var smooth:Bool = false;
	public var drawContainer:Bool = false;
	
	public function new(transMat:Matrix = null, colorTransform:ColorTransform = null, blendMode:String = null, smooth:Bool = false){
		colorTransform = null;
		transformMatrix = null;
		blendMode = BlendMode.NORMAL;
		smooth = false;
		drawContainer = false;
		
		this.transformMatrix = transMat;
		this.colorTransform = colorTransform;
		this.blendMode = blendMode;
		this.smooth = smooth;
		
		
	}
	
	public function draw(canvas:BitmapData, drawLayer:DisplayObject, transMat:Matrix = null, clipRect:Rectangle = null):void{
		
		var tMat:Matrix = transMat.clone();
		if(transformMatrix != null)
			tMat.concat(transformMatrix);
		
		
		canvas.draw(drawLayer, tMat, colorTransform, blendMode, clipRect, smooth);
	}
}
	
