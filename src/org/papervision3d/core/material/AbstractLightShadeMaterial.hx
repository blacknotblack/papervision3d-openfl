package org.papervision3d.core.material;
//import flash.utils.Map;


import org.papervision3d.core.material.TriangleMaterial;
import org.papervision3d.core.math.Matrix3D;
import org.papervision3d.core.proto.LightObject3D;
import org.papervision3d.core.render.data.RenderSessionData;
import org.papervision3d.core.render.draw.ITriangleDrawer;
import org.papervision3d.core.render.material.IUpdateBeforeMaterial;
import org.papervision3d.materials.utils.LightMatrix;
import org.papervision3d.objects.DisplayObject3D;

class AbstractLightShadeMaterial extends TriangleMaterial implements ITriangleDrawer implements IUpdateBeforeMaterial
{

	public var lightMatrices:Map<DisplayObject3D, Matrix3D>;
	private var _light:LightObject3D;
	private static var lightMatrix:Matrix3D;
	
	public function new()
	{
		super();
		init();
		
	}
	
	private function init():Void
	{
		lightMatrices = new Map<DisplayObject3D, Matrix3D>();
	}
	
	public function updateBeforeRender(renderSessionData:RenderSessionData):Void
	{	
		for(object in objects.keys()){
			var do3d:DisplayObject3D = untyped object;	
			lightMatrices.set(object, LightMatrix.getLightMatrix(light, do3d, renderSessionData, lightMatrices.get(object)));
		}
	}
	
	public var light(get_light, set_light):LightObject3D;
	public function set_light(light:LightObject3D):LightObject3D
	{
		_light = light;
		return _light;
	}
	
	public function get_light():LightObject3D
	{
		return _light;	
	}
	
}
