package org.papervision3d.core.render.sort;
using Lambda;

/**
 * @author Ralph Hauwert
 */
class BasicRenderSorter implements IRenderSorter
{
	public function new() {
		
	}
	
	//Sorts the renderlist by screenDepth.
	public function sort(array:Array<Dynamic>):Void
	{
		
		//array.sortOn("screenZ", Array.NUMERIC);
		array.sort(function(a:Dynamic, b:Dynamic):Int {
			if (a.screenZ > b.screenZ) {
				return 1;
			}
			else if (a.screenZ < b.screenZ) {
				return -1;
			}
			else {
				return 0;
			}
		});
	}
	
}
