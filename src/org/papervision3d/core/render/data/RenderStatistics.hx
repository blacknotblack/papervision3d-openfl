package org.papervision3d.core.render.data;
	
/**
 * @Author Ralph Hauwert
 */
 
class RenderStatistics
{
	public var projectionTime:Int;
	public var renderTime:Int;
	public var rendered:Int;
	public var triangles:Int;
	public var culledTriangles:Int;
	public var particles:Int;
	public var culledParticles:Int;
	public var lines:Int;
	public var shadedTriangles:Int;
	public var filteredObjects:Int;
	public var culledObjects:Int;
	
	public function new()
	{
		projectionTime = 0;
		renderTime = 0;
		rendered = 0;
		triangles = 0;
		culledTriangles = 0;
		particles = 0;
		culledParticles = 0;
		lines = 0;
		shadedTriangles = 0;
		filteredObjects = 0;
		culledObjects = 0;
	}
	
	public function clear():Void
	{
		projectionTime = 0;
		renderTime = 0;
		rendered = 0;
		particles = 0;
		triangles = 0;
		culledTriangles = 0;
		culledParticles = 0;
		lines = 0;
		shadedTriangles = 0;
		filteredObjects = 0;
		culledObjects = 0;
	}
	
	public function clone():RenderStatistics
	{
		var rs:RenderStatistics = new RenderStatistics();
		rs.projectionTime = projectionTime;
		rs.renderTime = renderTime;
		rs.rendered = rendered;
		rs.particles = particles;
		rs.triangles = triangles;
		rs.culledTriangles = culledTriangles;
		rs.lines = lines;
		rs.shadedTriangles = shadedTriangles;
		rs.filteredObjects = filteredObjects;
		rs.culledObjects = culledObjects;
		return rs;
	}
	
	public function toString():String
	{
		return ("ProjectionTime:"+projectionTime+" RenderTime:"+renderTime+" Particles:"+particles+" CulledParticles :"+culledParticles+" Triangles:"+triangles+" ShadedTriangles :"+shadedTriangles+" CulledTriangles:"+culledTriangles+" FilteredObjects:"+filteredObjects+" CulledObjects:"+culledObjects+"");
	}
	
}
