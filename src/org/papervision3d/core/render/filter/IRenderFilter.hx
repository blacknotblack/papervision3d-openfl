package org.papervision3d.core.render.filter;

import org.papervision3d.core.render.command.RenderableListItem;
	
/**
 * @Author Ralph Hauwert
 */
 
interface IRenderFilter
{	
	function filter(array:Array<RenderableListItem>):Int;
}
