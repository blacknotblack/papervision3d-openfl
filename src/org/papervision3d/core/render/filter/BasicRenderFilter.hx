package org.papervision3d.core.render.filter;
import org.papervision3d.core.render.command.RenderableListItem;
	
/**
 * @Author Ralph Hauwert
 */
class BasicRenderFilter implements IRenderFilter
{
	public function new() {
		
	}
	
	public function filter(array:Array<RenderableListItem>):Int
	{
		return 0;
	}
	
}
