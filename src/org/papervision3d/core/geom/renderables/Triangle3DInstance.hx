package org.papervision3d.core.geom.renderables;
import openfl.display.Sprite;

import org.papervision3d.core.math.Number3D;
import org.papervision3d.objects.DisplayObject3D;

class Triangle3DInstance
{
	public var instance:DisplayObject3D;
	
	/**
	* container is initialized via DisplayObject3D's render method IF DisplayObject3D.faceLevelMode is set to true
	*/
	public var container:Sprite;
	public var visible:Bool;
	public var screenZ:Float;
	public var faceNormal:Number3D;
	
	public function new(face:Triangle3D, instance:DisplayObject3D)
	{
		visible = false;
		this.instance = instance;
		faceNormal = new Number3D();
	}
}
