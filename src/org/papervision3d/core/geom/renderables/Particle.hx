package org.papervision3d.core.geom.renderables;
	
	
import openfl.geom.Matrix;
import openfl.geom.Rectangle;

import org.papervision3d.core.render.command.IRenderListItem;
import org.papervision3d.core.render.command.RenderParticle;
import org.papervision3d.materials.special.ParticleMaterial;	 

/**
 * This is the single renderable Particle object, used by Particles.as
 * 
 * See Particles.as for a full explanation. 
 * 
 * 
 * @author Ralph Hauwert
 * @author Seb Lee-Delisle
 */


class Particle extends AbstractRenderable implements IRenderable
{
	/**
	 * The size or scale factor of the particle.  
	 */		
	public var size:Float;
	public var vertex3D:Vertex3D;
	public var material:ParticleMaterial;
	public var renderCommand:RenderParticle;
	public var renderScale:Float;
	public var drawMatrix : Matrix; 
	public var rotationZ :Float; 
	
	/**
	 * The rectangle containing the particles visible area in 2D.  
	 */		
	public var renderRect:Rectangle;
	
	/**
	 * 
	 * @param material		The ParticleMaterial used for rendering the Particle
	 * @param size			The size of the particle. For some materials (ie BitmapParticleMaterial) this is used as a scale factor. 
	 * @param x				x position of the particle
	 * @param y				y position of the particle
	 * @param z				z position of the particle
	 * 
	 */		
	public function new(material:ParticleMaterial, size:Float = 1, x:Float = 0, y:Float = 0, z:Float = 0)
	{
		super();
		
		rotationZ = 0;
		
		this.material = material;
		this.size = size;
		this.renderCommand = new RenderParticle(this);
		this.renderRect = new Rectangle();
		vertex3D = new Vertex3D(x,y,z);
		drawMatrix  = new Matrix(); 
	}
	
	/**
	 * This is called during the projection cycle. It updates the rectangular area that 
	 * the particle is drawn into. It's important for the culling phase, and changes dependent
	 * on the type of material used.  
	 *  
	 */		

	public function updateRenderRect():Void
	{
		material.updateRenderRect(this);
	}
	
	public var x(get_x, set_x):Float;
	private function set_x(x:Float):Float
	{
		vertex3D.x = x;
		return x;
	}
	
	private function get_x():Float
	{
		return vertex3D.x;
	}
	
	public var y(get_y, set_y):Float;
	private function set_y(y:Float):Float
	{
		vertex3D.y = y;
		
		return y;
	}
	
	private function get_y():Float
	{
		return vertex3D.y;
	}
	
	public var z(get_z, set_z):Float;
	private function set_z(z:Float):Float
	{
		vertex3D.z = z;
		return z;
	}
	
	private function get_z():Float
	{
		return vertex3D.z;
	}
	
	override public function getRenderListItem():IRenderListItem
	{
		return renderCommand;
	}
	
}