/**
* @author Trevor McCauley
* @link www.senocular.com
*/
package org.papervision3d.core.utils.virtualmouse;
import openfl.display.InteractiveObject;
import openfl.events.MouseEvent;

/**
 * Wrapper for the MouseEvent class to let you check
 * to see if an event originated from the user's mouse
 * or a VirtualMouse instance.
 */
class VirtualMouseMouseEvent extends MouseEvent implements IVirtualMouseEvent {
	public function new(type:String, bubbles:Bool = false, cancelable:Bool = false, ?localX:Float=null, ?localY:Float=null, relatedObject:InteractiveObject = null, ctrlKey:Bool = false, altKey:Bool = false, shiftKey:Bool = false, buttonDown:Bool = false, delta:Int = 0){
		super(type, bubbles, cancelable, localX, localY, relatedObject, ctrlKey, altKey, shiftKey, buttonDown, delta);	
	}
}