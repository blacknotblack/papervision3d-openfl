package org.papervision3d.core.culling;
//import flash.utils.Map;

import org.papervision3d.objects.DisplayObject3D;



/**
 * @Author Ralph Hauwert
 */
class ViewportObjectFilter implements IObjectCuller
{
	
	private var _mode:Int;
	private var objects:Map<DisplayObject3D, DisplayObject3D>;
	
	public function ViewportObjectFilter(mode:Int):Void
	{
		this.mode = mode;
		init();
	}
	
	private function init():Void
	{
		objects = new Map<DisplayObject3D, DisplayObject3D>();
	}
	
	public function testObject(object:DisplayObject3D):Int
	{
		if(objects.get(object) != null){
			return 1-_mode;
		}else{
			return mode;
		}
		return 0;
	}
	
	public function addObject(do3d:DisplayObject3D):Void
	{
		objects.set(do3d, do3d);
	}
	
	public function removeObject(do3d:DisplayObject3D):Void
	{
		//delete objects[do3d];
		objects.remove(do3d);		
	}
	
	public var mode(get_mode, set_mode):Int;
	private function set_mode(mode:Int):Int
	{
		_mode = mode;	
		return mode;
	}
	
	private function get_mode():Int
	{
		return _mode;
	}
	
	public function destroy():Void
	{
		objects = null;
	}
	
}
