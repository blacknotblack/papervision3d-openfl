package org.papervision3d.view.layer;

import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.DisplayObject;
import openfl.display.Sprite;
import openfl.geom.Matrix;
import openfl.geom.Point;
import openfl.geom.Rectangle;

import org.papervision3d.core.effects.AbstractEffect;
import org.papervision3d.core.effects.utils.BitmapClearMode;
import org.papervision3d.core.effects.utils.BitmapDrawCommand;
import org.papervision3d.objects.DisplayObject3D;
import org.papervision3d.view.Viewport3D;

class BitmapEffectLayer extends ViewportLayer
{
	
	public var canvas:BitmapData;
	private var transMat:Matrix;
	public var clearMode:String;
	public var clippingRect:Rectangle;
	public var clippingPoint:Point;
	public var drawCommand:BitmapDrawCommand;
	public var clearBeforeRender:Bool;
	public var bitmapContainer:Bitmap;
	private var _width:Float;
	private var _height:Float;
	
	public var trackingObject:DisplayObject3D;
	public var trackingOffset:Point;
	
	public var scrollX:Float;
	public var scrollY:Float;
	
	public var effects:Array<AbstractEffect>;
	public var drawLayer:Sprite;
	public var renderAbove:Bool = false;
	
	public function BitmapEffectLayer(viewport:Viewport3D, w:Float = 640, h:Float=480, transparent:Bool = true, fillColor:Int=0, clearMode:String = "clear_pre", renderAbove:Bool = false, clearBeforeRender:Bool = false)
	{
		clearMode = BitmapClearMode.CLEAR_PRE;
		scrollX = 0;
		scrollY = 0;
		
		super(viewport, new DisplayObject3D(), false);
		
		effects = [];
		canvas = new BitmapData(w, h, transparent, fillColor);
		
		_width = w;
		_height = h;
		
		transMat = new Matrix();
		transMat.translate(w>>1, h>>1);
		
		bitmapContainer = new Bitmap(canvas);
		addChild(bitmapContainer);
		
		bitmapContainer.x = -(w*0.5);
		bitmapContainer.y = -(h*0.5);
		
		drawLayer = new Sprite();
		addChild(drawLayer);
		
		this.graphicsChannel = drawLayer.graphics;
		
		this.clearMode = clearMode;
		
		trackingOffset = new Point();
		clippingPoint = new Point();
		clippingRect = canvas.rect;
		
		drawCommand = new BitmapDrawCommand();
		
		this.clearBeforeRender = clearBeforeRender;
		if(!renderAbove)
			setChildIndex(drawLayer, 0);
	}
	
	public function setBitmapOffset(x:Float, y:Float):Void{
		
		bitmapContainer.x = x-(_width*0.5);
		bitmapContainer.y = y-(_height*0.5);
		
		transMat = new Matrix();
		transMat.translate(_width>>1, _height>>1);
		
		transMat.translate(-x, -y);
	}
	
	public function setTracking(object:DisplayObject3D, offset:Point = null):Void{
		trackingObject = object;
		if(offset)
			trackingOffset = offset;
		else
			trackingOffset = new Point();
	}
	
	public function setScroll(x:Float = 0, y:Float = 0):Void{
		scrollX = x;
		scrollY = y;
	}
	
	public function fillCanvas(color:Int):Void{
		canvas.fillRect(canvas.rect, color);
	}
	
	public function renderEffects():Void{

		var drawTarget:DisplayObject = drawLayer;
		
		if(trackingObject != null)
			setBitmapOffset(trackingObject.screen.x+trackingOffset.x, trackingObject.screen.y+trackingOffset.y);			
		
		if(drawCommand.drawContainer != null){
			drawTarget = this;
		}
		
		if(scrollX != 0 || scrollY != 0)
			canvas.scroll(scrollX, scrollY);
		
		drawCommand.draw(canvas, drawTarget, transMat, clippingRect);
		
		for (e in effects){
			e.postRender();
		}		
		if(clearMode == BitmapClearMode.CLEAR_POST)
			drawLayer.graphics.clear();
		
	}
	public function removeEffect(fx:AbstractEffect):Void{
		

	}
	
	public function setClipping(rect:Rectangle, point:Point):Void{
		this.clippingRect = rect;
		this.clippingPoint = point;
	}
	
	public function addEffect(fx:AbstractEffect):Void{
		
		fx.attachEffect(this);
		effects.push(fx);
		
	}
	
	public override function updateBeforeRender():Void
	{
		
		if(clearBeforeRender)
			canvas.fillRect(canvas.rect, 0);

		for(e in effects){
			e.preRender();
		}
		
		if(clearMode == BitmapClearMode.CLEAR_PRE)
			drawLayer.graphics.clear();
			
		super.updateBeforeRender();
	}
	
	public override function updateAfterRender():Void{
		//super.updateAfterRender();
		renderEffects();
	}
	
	public function getTranslationMatrix():Matrix{
		return transMat;
	}
	
}
		


