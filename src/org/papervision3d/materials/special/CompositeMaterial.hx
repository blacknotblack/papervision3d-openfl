package org.papervision3d.materials.special;
import openfl.display.BitmapData;
import openfl.display.Graphics;
import openfl.geom.Matrix;

import org.papervision3d.core.geom.renderables.Triangle3D;
import org.papervision3d.core.material.TriangleMaterial;
import org.papervision3d.core.proto.MaterialObject3D;
import org.papervision3d.core.render.command.RenderTriangle;
import org.papervision3d.core.render.data.RenderSessionData;
import org.papervision3d.core.render.draw.ITriangleDrawer;
import org.papervision3d.objects.DisplayObject3D;

using Lambda;

class CompositeMaterial extends TriangleMaterial implements ITriangleDrawer
{	
	public var materials:Array<MaterialObject3D>;
	
	public function new()
	{
		super();
		
		init();
	}
	
	private function init():Void
	{
		materials = [];
	}
	
	public function addMaterial(material:MaterialObject3D):Void
	{
		materials.push(material);
		for(object in objects.keys()){
			var do3d:DisplayObject3D = object;
			material.registerObject(do3d);
		}
	}
	
	public function removeMaterial(material:MaterialObject3D):Void
	{
		materials.splice(materials.indexOf(material),1);
	}
	
	public function removeAllMaterials():Void
	{
		materials = [];
	}
	
	override public function registerObject(displayObject3D:DisplayObject3D):Void
	{
		super.registerObject(displayObject3D);
		for(material in materials){
			material.registerObject(displayObject3D);
		}
	}
	
	override public function unregisterObject(displayObject3D:DisplayObject3D):Void
	{
		super.unregisterObject(displayObject3D);
		for(material in materials){
			material.unregisterObject(displayObject3D);
		}
	}
	
	override public function drawTriangle(tri:RenderTriangle, graphics:Graphics, renderSessionData:RenderSessionData, altBitmap:BitmapData=null, altUV:Matrix=null):Void{
		for(n in materials){
			if(!n.invisible){
				n.drawTriangle(tri, graphics, renderSessionData);
			}
		}
	}
	
}
