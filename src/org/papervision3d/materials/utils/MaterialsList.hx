﻿package org.papervision3d.materials.utils;
//import flash.utils.Map;

//import org.papervision3d.core.proto.*;

import org.papervision3d.core.proto.MaterialObject3D;

/**
* The MaterialsList class contains a list of materials.
* <p/>
* Each MaterialsList object has its own materials list.
*/
class MaterialsList
{
	
	/**
	* [internal-use] List of materials.
	*/
	private var _materials       :Map<MaterialObject3D, String>;

	private   var _materialsTotal  :Int;
	
	/**
	* List of materials indexed by name.
	*/
	public var materialsByName :Map<String, MaterialObject3D>;

	/**
	* Returns the number of children of this object.
	*/
	public var numMaterials(get_numMaterials, null):Int;
	public function get_numMaterials():Int
	{
		return _materialsTotal;
	}
	
	/**
	* Creates a list of materials.
	*
	* @param	materials	An array or name indexed object with materials to populate the list with.
	*/
	public function new( materials :Dynamic = null ):Void
	{
		this.materialsByName  = new Map<String,MaterialObject3D>();
		this._materials       = new Map<MaterialObject3D, String>();
		this._materialsTotal  = 0;

		if( materials != null)
		{
			if( Std.is(materials, Array) )
			{
				var arr:Array<MaterialObject3D> = untyped materials;
				for( i in arr )
				{
					this.addMaterial( i );
				} 
			}
			else if( Std.is(materials, Dynamic) )
			//else 
			{
				//var m:Hash<Dynamic> = untyped materials; 
				for ( name in Reflect.fields(materials))
				{
					//trace(name);
					this.addMaterial( Reflect.field(materials, name), name );
				}
			}
		}
	}

	/**
	* Adds a material to this MaterialsList object.
	*
	* @param	material	The material to add.
	* @param	name		An optional name of the material. If no name is provided, the material name will be used.
	* @return	The material you have added.
	*/
	public function addMaterial( material:MaterialObject3D, name:String=null ):MaterialObject3D
	{
		name = name != null? name : (material.name != null? material.name : Std.string( material.id ));
	
		this._materials.set( material , name);
		this.materialsByName.set( name , material);
		this._materialsTotal++;

		return material;
	}

	/**
	* Removes the specified material from the materials list.
	*
	* @param	material	The material to remove.
	* @return	The material you have removed.
	*/
	public function removeMaterial( material:MaterialObject3D ):MaterialObject3D
	{
		if(this._materials.get( material ) != null){
			this.materialsByName.remove( this._materials.get( material ) );
			this._materials.remove( material );
			_materialsTotal--;
		}
		return material;
	}

	/**
	* Returns the material that exists with the specified name.
	* </p>
	* @param	name	The name of the material to return.
	* @return	The material with the specified name.
	*/
	public function getMaterialByName( name:String ):MaterialObject3D
	{
		return this.materialsByName.get(name) != null ? this.materialsByName.get(name) : this.materialsByName.get("all");
		//return this.materialsByName[ name ];
	}

	/**
	* Removes the material that exists with the specified name.
	* </p>
	* The material object is garbage collected if no other references to the material exist.
	* </p>
	* The garbage collector is the process by which Flash Player reallocates unused memory space. When a variable or object is no longer actively referenced or stored somewhere, the garbage collector sweeps through and wipes out the memory space it used to occupy if no other references to it exist.
	* </p>
	* @param	name	The name of the material to remove.
	* @return	The material object that was removed.
	*/
	public function removeMaterialByName( name:String ):MaterialObject3D
	{
		return removeMaterial( getMaterialByName( name ) );
	}

	/**
	* Creates a copy of the materials list.
	*
	* @return	A newly created materials list that contains a duplicate of each of its materials.
	*/
	public function clone():MaterialsList
	{
		var cloned:MaterialsList = new MaterialsList();

		for( m in this.materialsByName )
			cloned.addMaterial( m.clone(), this._materials.get( m ) );
		
		return cloned;
	}

	/**
	* Returns a string with the names of the materials in the list.
	*
	* @return	A string.
	*/
	public function toString():String
	{
		var list:String = "";
		
		for( m in this.materialsByName )
			list += this._materials.get( m ) + "\n";

		return list;
	}
}