package org.papervision3d.materials.utils;
import flash.geom.Matrix;

import org.papervision3d.core.geom.renderables.Vertex3DInstance;

class RenderRecStorage
{
	public var v0:Vertex3DInstance;
	public var v1:Vertex3DInstance;
	public var v2:Vertex3DInstance;
	public var mat:Matrix;
	
	public function new()
	{
		v0 = new Vertex3DInstance();
		v1 = new Vertex3DInstance();
		v2 = new Vertex3DInstance();
		mat = new Matrix();
	}

}
