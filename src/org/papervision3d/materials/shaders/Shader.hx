package org.papervision3d.materials.shaders;
import openfl.display.BitmapData;
import openfl.display.BlendMode;
import openfl.display.Sprite;
import openfl.events.EventDispatcher;
import openfl.filters.BitmapFilter;
//


import org.papervision3d.core.geom.renderables.Triangle3D;
import org.papervision3d.core.render.data.RenderSessionData;
import org.papervision3d.core.render.shader.ShaderObjectData;
import org.papervision3d.objects.DisplayObject3D;

/**
 * @Author Ralph Hauwert
 */
class Shader extends EventDispatcher implements IShader
{
	private var _filter:BitmapFilter;
	private var _blendMode:BlendMode;
	private var _object:DisplayObject3D;
	private var layers:Map<DisplayObject3D, Sprite>;
	
	public function new()
	{
		_blendMode = BlendMode.MULTIPLY;
		super();
		this.layers = new Map<DisplayObject3D, Sprite>();
	}
			
	public function renderLayer(triangle:Triangle3D, renderSessionData:RenderSessionData, sod:ShaderObjectData):Void
	{
		
	}
	
	public function renderTri(triangle:Triangle3D, renderSessionData:RenderSessionData, sod:ShaderObjectData, bmp:BitmapData):Void
	{
		
	}
	
	public function destroy():Void
	{
		
	}
	
	public function setContainerForObject(object:DisplayObject3D, layer:Sprite):Void
	{
		layers.set(object, layer);
	}
	
	public var filter(get_filter, set_filter):BitmapFilter;
	private function set_filter(filter:BitmapFilter):BitmapFilter
	{
		_filter = filter;
		return filter;
	}
	
	private function get_filter():BitmapFilter
	{
		return _filter;	
	}
	
	public var layerBlendMode(get_layerBlendMode, set_layerBlendMode):BlendMode;
	private function set_layerBlendMode(blendMode:BlendMode):BlendMode
	{
		_blendMode = blendMode;
		return blendMode;
	}
	
	private function get_layerBlendMode():BlendMode
	{
		return _blendMode;
	}
	
	public function updateAfterRender(renderSessionData:RenderSessionData, sod:ShaderObjectData):Void
	{
		
	}
	
}