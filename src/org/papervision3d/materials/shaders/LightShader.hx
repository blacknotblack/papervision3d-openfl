package org.papervision3d.materials.shaders;
import org.papervision3d.core.proto.LightObject3D;
import org.papervision3d.core.render.data.RenderSessionData;
import org.papervision3d.core.render.shader.ShaderObjectData;
import org.papervision3d.materials.utils.LightMatrix;

/**
 * @Author Ralph Hauwert
 */
class LightShader extends Shader implements IShader implements ILightShader
{

	public function LightShader():Void
	{
		super();	
	}
	
	
	public var light(get_light, set_light):LightObject3D;
	private function set_light(light:LightObject3D):LightObject3D
	{
		_light = light;
		return _light;
	}
	
	private function get_light():LightObject3D
	{
		return _light;	
	}
	
	public function updateLightMatrix(sod:ShaderObjectData, renderSessionData:RenderSessionData):Void
	{
		sod.lightMatrices.set(this, LightMatrix.getLightMatrix(light, sod.object, renderSessionData, sod.lightMatrices.get(this));
	}
	
	private var _light:LightObject3D;
	
}
