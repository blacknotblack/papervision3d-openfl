/*
nochump.util.zip.ZipConstants
Copyright(C)2007 David Chang(dchang@nochump.com)

This file is part of nochump.util.zip.

nochump.util.zip is free software:you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option)any later version.

nochump.util.zip is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY;without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Foobar.  If not, see<http://www.gnu.org/licenses/>.
*/
package nochump.util.zip;

internal class ZipConstants {
	
	/* The local file header */
	internal static var LOCSIG:Int=0x04034b50;	// "PK\003\004"
	internal static var LOCHDR:Int=30;	// LOC header size
	internal static var LOCVER:Int=4;	// version needed to extract
	//internal static var LOCFLG:Int=6;// general purpose bit flag
	//internal static var LOCHOW:Int=8;// compression method
	//internal static var LOCTIM:Int=10;// modification time
	//internal static var LOCCRC:Int=14;// uncompressed file crc-32 value
	//internal static var LOCSIZ:Int=18;// compressed size
	//internal static var LOCLEN:Int=22;// uncompressed size
	internal static var LOCNAM:Int=26;// filename length
	//internal static var LOCEXT:Int=28;// extra field length
	
	/* The Data descriptor */
	internal static var EXTSIG:Int=0x08074b50;	// "PK\007\008"
	internal static var EXTHDR:Int=16;	// EXT header size
	//internal static var EXTCRC:Int=4;// uncompressed file crc-32 value
	//internal static var EXTSIZ:Int=8;// compressed size
	//internal static var EXTLEN:Int=12;// uncompressed size
	
	/* The central directory file header */
	internal static var CENSIG:Int=0x02014b50;	// "PK\001\002"
	internal static var CENHDR:Int=46;	// CEN header size
	//internal static var CENVEM:Int=4;// version made by
	internal static var CENVER:Int=6;// version needed to extract
	//internal static var CENFLG:Int=8;// encrypt, decrypt flags
	//internal static var CENHOW:Int=10;// compression method
	//internal static var CENTIM:Int=12;// modification time
	//internal static var CENCRC:Int=16;// uncompressed file crc-32 value
	//internal static var CENSIZ:Int=20;// compressed size
	//internal static var CENLEN:Int=24;// uncompressed size
	internal static var CENNAM:Int=28;// filename length
	//internal static var CENEXT:Int=30;// extra field length
	//internal static var CENCOM:Int=32;// comment length
	//internal static var CENDSK:Int=34;// disk number start
	//internal static var CENATT:Int=36;// Internal file attributes
	//internal static var CENATX:Int=38;// external file attributes
	internal static var CENOFF:Int=42;// LOC header offset
	
	/* The entries in the end of central directory */
	internal static var ENDSIG:Int=0x06054b50;	// "PK\005\006"
	internal static var ENDHDR:Int=22;// END header size
	//internal static var ENDSUB:Int=8;// number of entries on this disk
	internal static var ENDTOT:Int=10;	// total number of entries
	//internal static var ENDSIZ:Int=12;// central directory size in bytes
	internal static var ENDOFF:Int=16;// offset of first CEN header
	//internal static var ENDCOM:Int=20;// zip file comment length
	
	/* Compression methods */
	internal static var STORED:Int=0;
	internal static var DEFLATED:Int=8;
	
}