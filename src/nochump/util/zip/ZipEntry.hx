/*
nochump.util.zip.ZipEntry
Copyright(C)2007 David Chang(dchang@nochump.com)

This file is part of nochump.util.zip.

nochump.util.zip is free software:you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option)any later version.

nochump.util.zip is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY;without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Foobar.  If not, see<http://www.gnu.org/licenses/>.
*/
package nochump.util.zip;

import openfl.utils.ByteArray<Dynamic>;

/**
 * This class represents a member of a zip archive.  ZipFile
 * will give you instances of this class as information
 * about the members in an archive.  On the other hand ZipOutput
 * needs an instance of this class to create a new member.
 *
 * @author David Chang
 */
class ZipEntry {
	
	// some members are Internal as ZipFile will need to set these directly
	// where their accessor does type conversion
	private var _name:String;
	private var _size:Int=-1;
	private var _compressedSize:Int=-1;
	private var _crc:Int;
	/** @private */
	private var dostime:Int;
	private var _method:Int=-1;// compression method
	private var _extra:ByteArray<Dynamic>;// optional extra field data for entry
	private var _comment:String;// optional comment string for entry
	// The following flags are used only by ZipOutput
	/** @private */
	private var flag:Int;// bit flags
	/** @private */
	private var version:Int;// version needed to extract
	/** @private */
	private var offset:Int;// offset of loc header
	
	/**
	 * Creates a zip entry with the given name.
	 * @param name the name. May include directory components separated
	 * by '/'.
	 */
	public function new(name:String){
		_name=name;
	}
	
	/**
	 * Returns the entry name.  The path components in the entry are
	 * always separated by slashes('/').  
	 */
	public var name(get_name, set_name):String;
 	private function get_name():String {
		return _name;
	}
	
	/**
	 * Gets the time of last modification of the entry.
	 * @return the time of last modification of the entry, or -1 if unknown.
	 */
	public var time(get_time, set_time):Float;
 	private function get_time():Float {
		var d:Date=new Date(
			((dostime>>25)& 0x7f)+ 1980,
			((dostime>>21)& 0x0f)- 1,
			(dostime>>16)& 0x1f,
			(dostime>>11)& 0x1f,
			(dostime>>5)& 0x3f,
			(dostime & 0x1f)<<1
		);
		return d.time;
	}
	/**
	 * Sets the time of last modification of the entry.
	 * @time the time of last modification of the entry.
	 */
	private function set_time(time:Float):Void {
		var d:Date=new Date(time);
		dostime=
			(d.fullYear - 1980 & 0x7f)<<25
			|(d.month + 1)<<21
			| d.day<<16
			| d.hours<<11
			| d.minutes<<5
			| d.seconds>>1;
	}
	
	/**
	 * Gets the size of the uncompressed data.
	 */
	public var size(get_size, set_size):Int;
 	private function get_size():Int {
		return _size;
	}
	/**
	 * Sets the size of the uncompressed data.
	 */
	private function set_size(size:Int):Void {
		_size=size;
	}
	
	/**
	 * Gets the size of the compressed data.
	 */
	public var compressedSize(get_compressedSize, set_compressedSize):Int;
 	private function get_compressedSize():Int {
		return _compressedSize;
	}
	/**
	 * Sets the size of the compressed data.
	 */
	private function set_compressedSize(csize:Int):Void {
		_compressedSize=csize;
	}
	
	/**
	 * Gets the crc of the uncompressed data.
	 */
	public var crc(get_crc, set_crc):Int;
 	private function get_crc():Int {
		return _crc;
	}
	/**
	 * Sets the crc of the uncompressed data.
	 */
	private function set_crc(crc:Int):Void {
		_crc=crc;
	}
	
	/**
	 * Gets the compression method. 
	 */
	public var method(get_method, set_method):Int;
 	private function get_method():Int {
		return _method;
	}
	/**
	 * Sets the compression method.  Only DEFLATED and STORED are
	 * supported.
	 */
	private function set_method(method:Int):Void {
		_method=method;
	}
	
	/**
	 * Gets the extra data.
	 */
	public var extra(get_extra, set_extra):ByteArray;
 	private function get_extra():ByteArray {
		return _extra;
	}
	/**
	 * Sets the extra data.
	 */
	private function set_extra(extra:ByteArray):Void {
		_extra=extra;
	}
	
	/**
	 * Gets the extra data.
	 */
	public var comment(get_comment, set_comment):String;
 	private function get_comment():String {
		return _comment;
	}
	/**
	 * Sets the entry comment.
	 */
	private function set_comment(comment:String):Void {
		_comment=comment;
	}
	
	/**
	 * Gets true, if the entry is a directory.  This is solely
	 * determined by the name, a trailing slash '/' marks a directory.  
	 */
	public function isDirectory():Bool {
		return _name.charAt(_name.length - 1)=='/';
	}
	
	/**
	 * Gets the string representation of this ZipEntry.  This is just
	 * the name as returned by name.
	 */
	public function toString():String {
		return _name;
	}
	
}